import copy
import logging
import os

import requests

from exceptionFile import GrafanaError, GrafanaUnreachableError, GrafanaImageError
from grafana_dashboard import GrafanaDashboard
from copy import deepcopy
from datetime import datetime, timedelta
from requests.exceptions import ConnectionError



# Holt das Datum/Zeit von vor 6h und konvertiert es in Integer
def fromDateTimeConverter():
    currentDate = datetime.now()
    delta = timedelta(hours=6)
    dateValue = currentDate - delta
    return int(dateValue.timestamp())


# Holt das jetzige Datum/Zeit und konvertiert es in Integer
def toDateTimeConverter():
    currentDate = datetime.now()
    return int(currentDate.timestamp())


class ImageFetcher:
    """
    Gets the pictures of all panels from Grafana and saves them in their objects
    """

    # Initialisierung der Attribute
    def __init__(self):
        """
        Attributes:
            authorization: Bearer token
        """
        self.authorization: str = ""
        self.start: int = 1667131272
        self.end: int = 1667217672
        self.from_param: int = fromDateTimeConverter()
        self.to_param: int = toDateTimeConverter()
        self.match: str = "match[]"
        self.scheme: str = "http://"
        self.host_name: str = os.environ.get("GRAFANA_HOST", "localhost:3000")
        self.extension: str = "/api/dashboards/uid/"
        self.repeatExtension: str = "/api/datasources/1/resources/api/v1/series"
        self.imageExtension: str = "/render/d-solo/"
        self.dashboard_uid: str = ""
        self.dashboard_title: str = ""
        self.variables: list = []
        self.org_id: int = 1
        self.img_width: int = 1000
        self.img_height: int = 500
        self.dashboard = GrafanaDashboard()

    @classmethod
    def fetchGrafana(cls, url, params=None, headers=None) -> dict:
        try:
            r = requests.get(url, headers=headers, params=params)
            if not r.ok:
                grafana_error = r.json()
                raise GrafanaError(grafana_error["message"], r.status_code)
            logging.info(r.status_code)
            return r.json()
        except ConnectionError:
            raise GrafanaUnreachableError("Couldn't connect to Grafana: {}".format(url), 405)

    @classmethod
    def fetchRenderer(cls, url, params=None, headers=None, timeout=None):
        try:
            r = requests.get(url, headers=headers, params=params, timeout=timeout)
            if not r.ok:
                raise GrafanaError("Couldn't fetch following PanelImage: {}".format(url), r.status_code)
            logging.info(r.status_code)
            return r.content
        except ConnectionError:
            raise GrafanaImageError("Couldn't fetch following PanelImage: {}. Please check if render-engine is working!"
                                    .format(url), 405)

    # Aufbau der Headers
    def buildHeader(self):
        return {"Authorization": self.authorization,
                "Params": self.match + "node_filesystem_files_free start " + str(self.start) + " end " + str(self.end)}

    # Ladet das Dashboard von der JSON-Datei
    def loadDashboardJson(self):
        headers = self.buildHeader()
        r = self.fetchGrafana(self.scheme + self.host_name + self.extension + self.dashboard_uid + "?", headers=headers)
        return r

    def buildParams(self, match=None, isMillis=False, **kwargs):
        params = copy.deepcopy(self.variables)
        if match is not None:
            params.append(("match[]", str(match)))
        if isMillis:
            params.append(("start", str(self.from_param * 1000)))
            params.append(("end", str(self.to_param * 1000)))
        else:
            params.append(("start", str(self.from_param)))
            params.append(("end", str(self.to_param)))

        for key, value in kwargs.items():
            params.append((key, value))
        return params

    # Überprüft das jetzige Panel auf mögliche Duplikate
    def checkIfRepeat(self):
        maxId = 0
        r = self.loadDashboardJson()
        self.dashboard = GrafanaDashboard(r)

        for panel in self.dashboard.panels:
            if panel.is_copy:
                logging.info("panel copy: " + str(panel.id))
            else:
                if panel.id > maxId:
                    maxId = panel.id
                if panel.repeat != "":
                    query = panel.getRepeatQuery(self.dashboard.templating)
                    logging.info("Query = " + str(query))
                    params = self.buildParams(query)
                    url = (self.scheme + self.host_name + self.repeatExtension)

                    u = self.fetchGrafana(url=url, headers=self.buildHeader(), params=params)
                    filteredData = []
                    for data in u["data"]:
                        found = False
                        for t in self.variables:
                            t_without_var = t[0].replace("var-", "")
                            if t_without_var in data and t[1] == data[t_without_var]:
                                found = True

                        if found:
                            filteredData.append(data)

                    originalTitle = panel.title
                    panel.sectionTitle = panel.title.replace("$" + panel.repeat, "")
                    if len(u["data"]) > 0:
                        panel.title = panel.title.replace("$" + panel.repeat,
                                                          u["data"][0][panel.repeat])
                    else:
                        panel.title = panel.title.replace("$" + panel.repeat, "undefined")

                    for duplicate_index in range(1, len(filteredData)):
                        maxId += 1
                        duplicate = deepcopy(panel)
                        duplicate.id = maxId
                        duplicate.title = originalTitle.replace("$" + panel.repeat,
                                                                filteredData[duplicate_index][panel.repeat])
                        duplicate.is_copy = True
                        panel.duplicates.append(duplicate)
                        logging.info("Duplicate: " + str(duplicate.id) + " generated")

    # Holt von jedem Panel/Duplikat das entsprechende Bild und fügt es dessen Objekt zu
    def getPanelImage(self):
        for panel in self.dashboard.panels:
            params = self.buildParams(orgId=self.org_id, panelId=panel.id, width=self.img_width, height=self.img_height,
                                      isMillis=True)
            url = (self.scheme + self.host_name + self.imageExtension + self.dashboard_uid + self.dashboard.title)
            logging.info("Trying to get panel{}".format(panel.id))
            panel.image_content = self.fetchRenderer(url=url, params=params,
                                                     headers={"Authorization": self.authorization}, timeout=30)
            if len(panel.duplicates) != 0:
                for duplicate in panel.duplicates:
                    params = self.buildParams(orgId=self.org_id, panelId=duplicate.id, width=self.img_width,
                                              height=self.img_height, isMillis=True)
                    logging.info("Trying to get panel{}".format(duplicate.id))
                    duplicate.image_content = self.fetchRenderer(url=url, params=params,
                                                                 headers={"Authorization": self.authorization},
                                                                 timeout=30)

    def process(self):
        self.loadDashboardJson()
        self.checkIfRepeat()
        self.getPanelImage()


if __name__ == '__main__':
    image_fetcher = ImageFetcher()
    image_fetcher.loadDashboardJson()
