class GrafanaError(Exception):
    """Exception raised for invalid grafana response"""

    def __init__(self, message, status_code):
        self.status_code = status_code
        self.message = message
        super().__init__(self.message)


class GrafanaUnreachableError(Exception):
    """Exception raised for invalid grafana response"""

    def __init__(self, message, status_code):
        self.status_code = status_code
        self.message = message
        super().__init__(self.message)


class GrafanaImageError(Exception):
    """Exception raised for invalid grafana response"""

    def __init__(self, message, status_code):
        self.status_code = status_code
        self.message = message
        super().__init__(self.message)
