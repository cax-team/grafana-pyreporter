import json
import logging
from typing import Optional


class GrafanaTargets:
    def __init__(self, dashboard_data: dict):
        self.expr = dashboard_data["expr"]
        self.refId = dashboard_data["refId"]


class GrafanaTemplating:
    def __init__(self, dashboard_data=None):
        if dashboard_data is None:
            dashboard_data = {}
        self.list = []
        if "list" in dashboard_data:
            for entry in dashboard_data["list"]:
                self.list.append(GrafanaList(entry))

    def __str__(self):
        str_rep = ""
        for entry in self.list:
            str_rep = str_rep + str(entry)
        return str_rep


class GrafanaList:
    def __init__(self, dashboard_data: dict):
        self.name = dashboard_data["name"]
        self.label = dashboard_data["label"]
        self.regex = dashboard_data["regex"]
        self.query = GrafanaQuery(dashboard_data["query"])

    def __str__(self):
        return "label = " + self.label


class GrafanaQuery:
    def __init__(self, dashboard_data: dict):
        self.query = dashboard_data["query"]
        self.refId = dashboard_data["refId"]

    def __str__(self):
        return self.query


class GrafanaDashboard:
    def __init__(self, dashboard_data=None):
        if dashboard_data is None:
            dashboard_data = {}
            self.panels: [GrafanaPanel] = []
            self.uid = ""
            self.title = ""
            self.id = -1
            self.templating = GrafanaTemplating()
        else:
            self.panels: [GrafanaPanel] = []
            self.uid = dashboard_data['dashboard']["uid"]
            self.title = dashboard_data['dashboard']["title"].replace("_", "-").replace("$", "")
            self.id = dashboard_data['dashboard']["id"]
            self.templating = GrafanaTemplating(dashboard_data['dashboard']["templating"])
            for panel in dashboard_data['dashboard']["panels"]:
                self.panels.append(GrafanaPanel(panel, self.templating))

    def __str__(self):
        return "uid: " + str(self.uid)


class GrafanaPanel:
    def __init__(self, dashboard_data: dict, templating):
        self.id: int = -1
        self.title: str = ""
        self.sectionTitle: str = ""
        self.repeat: str = ""
        self.repeatDirection: str = ""
        self.targets: [GrafanaTargets] = []
        self.duplicates: [GrafanaPanel] = []
        self.is_copy: bool = False
        self.image_content = None
        try:
            self.id = dashboard_data["id"]
            self.title = dashboard_data["title"]
            self.repeat = dashboard_data["repeat"]
            self.repeatDirection = dashboard_data["repeatDirection"]
            for target in dashboard_data["targets"]:
                self.targets.append(GrafanaTargets(target))
            self.getRepeatQuery(templating)
            self.image_content: bytearray = None
        except KeyError:
            logging.info("KeyError ID = " + str(self.id))
            pass

    def getRepeatQuery(self, templating) -> Optional[GrafanaQuery]:
        if self.repeat != "":
            for template in templating.list:
                if template.name == self.repeat:
                    return template.query


if __name__ == "__main__":
    with open("data.json", "r", encoding="utf-8") as f:
        json_content = json.load(f)
    dashboard = GrafanaDashboard(json_content)
    for panel in dashboard.panels:
        logging.info(panel.getRepeatQuery(dashboard.templating))
