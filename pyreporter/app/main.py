import logging

import requests
from requests import Timeout


def saveImage(panel_id, headers):
    logging.info("Trying to get panel{}".format(panel_id))
    r = requests.get("http://localhost:3000/render/d-solo/__hNFSN4z/node_filesystem_files_free-copy?orgId=1&from"
                     "=1666860579300&to=1666946979300&panelId={}&width=1000&height=500&".format(panel_id),
                     headers=headers, timeout=10)
    with open("test_panelId{}.png".format(panel_id), "wb") as f:
        f.write(r.content)


def print_status_quote():
    headers = {
        "Authorization": "Bearer eyJrIjoiWHZ6N0YwZmxrRWxyWDlSQkhMVHRtSFljVTl2ZENtWHoiLCJuIjoiYXBpdG9rZW4iLCJpZCI6MX0=",
        "Params": "match%5B%5D node_filesystem_files_free start 1667131272 end 1667217672"}
    r = requests.get('http://localhost:3000/api/dashboards/uid/__hNFSN4z?', headers=headers)

    logging.info(r.status_code)
    json_response = r.json()
    logging.info(json_response["dashboard"]["title"])
    logging.info("------------------")
    logging.info(headers)
    maxId = 0

    for panel in json_response["dashboard"]["panels"]:
        if panel["id"] > maxId:
            maxId = panel["id"]
        if "repeat" in panel:
            try:
                for template in json_response["dashboard"]["templating"]["list"]:
                    if template["name"] == panel["repeat"]:
                        query = template["query"]["query"]
                url = "http://localhost:3000/api/datasources/1/resources/api/v1/series?match%5B%5D={}&start=1667131272&end=1667217672".format(query)
                u = requests.get(url, headers=headers)
                json_responseU = u.json()
                logging.info(url)
                saveImage(panel_id=panel["id"], headers=headers)
                for duplicate in range(len(json_responseU["data"]) - 1):
                    maxId += 1
                    saveImage(panel_id=maxId, headers=headers)
            except Timeout:
                logging.info("---Timeout Exception---")
                continue
        else:
            saveImage(panel_id=panel["id"], headers=headers)


if __name__ == '__main__':
    print_status_quote()
