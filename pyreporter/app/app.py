from flask import Flask, send_from_directory
from flask_restx import Resource, Api, fields
from flask_sqlalchemy import SQLAlchemy
import os
from datetime import datetime, timedelta

from werkzeug.exceptions import MethodNotAllowed

import image_reporter
import logging

from exceptionFile import GrafanaError, GrafanaUnreachableError

basedir = os.path.dirname(os.path.realpath(__file__))
logging.info("basedir path: " + basedir)

app = Flask("__name__")
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(basedir, "../restx/books.db")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_ECHO"] = True
app.config['RESTX_VALIDATE'] = True
app.config['UPLOAD_FOLDER'] = os.path.join(os.getcwd(), r'upload')

api = Api(app, doc="/", title="Dashboard API")

db = SQLAlchemy(app)

variable_model = api.model(
    "variables",
    {
        "key": fields.String(),
        "value": fields.String(),
        "parent": fields.String(),
    }
)

dashboard_model = api.model(
    "dashboard",
    {
        "uid": fields.String(required=True, description="UID of the dashboard\nExample: __hNFSN3z"),
        "apiKey": fields.String(required=True, description="Grafana API Key\nExample: eyJrIjoiNzZUemxKd1lzVHJQYXc2NUI0MUdUOHM2NGtpZWhmUDMiLCJuIjoiYXBpS2V5IiwiaWQiOjF9"),
        "fromTime": fields.Integer(description="From when the panels should be meassured\nExample: 1672143573"),
        "toTime": fields.Integer(description="To when the panels should be meassured\nExample: 1672229973"),
        "variables": fields.List(fields.Nested(variable_model), description="Which duplicates should be printed")
    }
)


@api.errorhandler(MethodNotAllowed)
def InvalidMethod(error):
    return {'message': error.description, 'valid_methods': error.valid_methods}, 405


@api.errorhandler(GrafanaError)
@api.errorhandler(GrafanaUnreachableError)
def RequestConnectionError(error: GrafanaError):
    return {'message': error.message}, error.status_code


@api.route("/dashboard")
class DashboardApi(Resource):
    @api.doc(params={'id': 'An ID'})
    @api.response(200, 'PDF-File was created successfully')
    @api.response(400, 'Couldnt create PDF-File')
    @api.response(500, 'Couldnt create PDF-File')
    @api.expect(dashboard_model, validate=True)
    def post(self):
        """Create a PDF-file"""
        data = api.payload

        uid: str = data.get("uid")
        apiKey: str = data.get("apiKey")
        fromTime: int = data.get("fromTime")
        if fromTime == 0:
            currentDate = datetime.now()
            delta = timedelta(hours=6)
            dateValue = currentDate - delta
            fromTime = int(dateValue.timestamp())

        toTime: int = data.get("toTime")
        if toTime == 0:
            currentDate = datetime.now()
            toTime = int(currentDate.timestamp())
        variables = data.get("variables")

        testObj = image_reporter.ImageReporter()
        pdfFileName = testObj.createPDF(app.config['UPLOAD_FOLDER'], uid, apiKey, fromTime, toTime, variables)

        logging.info("PDF created")
        return {
            "file_name": pdfFileName
        }


@app.route("/file-download/<filename>")
def sendFile(filename):
    current_directory = os.getcwd()
    upload_directory = current_directory + "/upload"
    return send_from_directory(upload_directory, filename)


@app.route('/')
def hello_geek():
    return '<h1>Hello from Flask & Docker</h2>'


if __name__ == "__main__":
    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.makedirs(app.config['UPLOAD_FOLDER'])
    app.run(debug=True)
