import subprocess

def create_pdf():
    process = subprocess.Popen([
        'latex',
        '-output-format=pdf',
        '-job-name=' + "templateTestPDF",
        "template.tex"])
    process.wait()


create_pdf()
