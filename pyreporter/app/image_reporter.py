import shutil
import uuid
from datetime import datetime
import os
import subprocess

from image_fetcher import ImageFetcher


class ImageReporter:
    def __init__(self):
        self.image_fetcher = ImageFetcher()

    def variablesToString(self, variables):
        parameterList = []
        if variables is not None:
            for variable in variables:
                parameterList.append(("var-" + variable["key"], variable["value"]))
        return parameterList

    # Bestimmt alle mitgegebenen Attribute vom Image_Fetcher
    def loadParameters(self, uid: str, apiKey: str, fromTime: int, toTime: int, variables):
        self.image_fetcher.dashboard_uid = uid + "/"
        self.image_fetcher.authorization = "Bearer " + str(apiKey)
        if variables is not None:
            self.image_fetcher.variables = self.variablesToString(variables)

        # Sichere Eingabe für die Zeit
        if fromTime != 0 and len(str(fromTime)) == 10:
            self.image_fetcher.from_param = fromTime
        if toTime != 0 and len(str(toTime)) == 10:
            self.image_fetcher.to_param = toTime

        self.image_fetcher.process()

    # Kombiniert alle Lines
    @classmethod
    def extendListWithList(cls, templateContent, index: int, listToBeAdded):
        templateContentPart1 = templateContent[0:index]
        templateContentPart2 = templateContent[index + 1:len(templateContent)]
        templateContentPart1 += listToBeAdded
        templateContentPart1 += templateContentPart2
        return templateContentPart1

    # Erstellt aus dem .tex-File das entsprechende PDF-File
    @classmethod
    def pdfCompile(cls, file_name, path):
        splitted_filename = file_name.split('.')
        latex_format = 'pdf'
        if len(splitted_filename) > 1:
            file_name = splitted_filename[0]
            latex_format = splitted_filename[1]
        process = subprocess.Popen([
            'latex',
            '-output-format=' + latex_format,
            '-job-name=' + file_name,
            "main.tex"], cwd=path)
        process.wait()

    # Holt sich das entsprechende Bild von jedem Panel und setzt das PDF zusammen
    def createPDF(self, base_directory, uid, apiKey, fromTime, toTime, variables):
        self.loadParameters(uid, apiKey, fromTime, toTime, variables)

        pdf_uid = str(uuid.uuid4())
        temp_pdf_files = os.path.join(os.getcwd(), "output", pdf_uid)

        os.makedirs(temp_pdf_files)
        allGrafanaPanelsIndex = -1
        dataLineContent = []
        with open('template.tex', 'r+') as f:
            data = f.readlines()

        # Ändert den Titel ab
        for index, line in enumerate(data):
            if "templateTitle" in line:
                title = self.image_fetcher.dashboard.title.replace("_", " ")
                data[index] = line.replace("templateTitle", title)

        # Setzt das heutige Datum/Zeit
        for index, line in enumerate(data):
            if "todaysDate" in line:
                data[index] = line.replace("todaysDate", str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

        # Schreibt den Zeitspann hin, an dem die Daten gesammelt wurden
        for index, line in enumerate(data):
            if "abstractContent" in line:
                startTime = datetime.fromtimestamp(self.image_fetcher.from_param * 1000 / 1e3).strftime(
                    "%Y-%m-%d %H:%M:%S")
                endTime = datetime.fromtimestamp(self.image_fetcher.to_param * 1000 / 1e3).strftime("%Y-%m-%d %H:%M:%S")
                data[index] = line.replace("abstractContent",
                                           "Measured from: " + str(startTime) + " to: " + str(endTime))

        # Wenn "allGrafanaPanels" auf einer Line gefunden wird, wird folgendes ausgeführt
        for index, line in enumerate(data):
            if "allGrafanaPanels" in line:
                allGrafanaPanelsIndex = index

                # Bild jetziges Panel erstellen + einfügen
                for panel in self.image_fetcher.dashboard.panels:
                    image_path = os.path.join(temp_pdf_files, "image_panelId{}.png".format(panel.id)).replace('\\', '/')
                    image_path_content = image_path[:-4]
                    with open(image_path, "wb") as f:
                        f.write(panel.image_content)
                    if panel.repeat != "":
                        dataLineContent.append(line.replace("secContent", panel.sectionTitle)
                                               .replace("allGrafanaPanels", image_path_content)
                                               .replace("allGrafanaCaptions", panel.title))
                    else:
                        dataLineContent.append(line.replace("secContent", panel.title)
                                .replace("allGrafanaPanels", image_path_content)
                                .replace("allGrafanaCaptions", panel.title))

                    # Überprüfen, ob jetziges Panel Duplikate hat
                    if len(panel.duplicates) != 0:
                        # Für jedes Duplikat Bild erstellen + einfügen
                        for duplicate in panel.duplicates:
                            duplicate_path = os.path.join(temp_pdf_files, "image_panelId{}.png".format(
                                duplicate.id)).replace('\\', '/')
                            duplicate_path_content = duplicate_path[:-4]
                            with open(duplicate_path, "wb") as f:
                                f.write(duplicate.image_content)
                            dataLineContent.append(
                                line.replace("\section", "").replace("{secContent}", "").replace(
                                    "allGrafanaPanels", duplicate_path_content).replace("allGrafanaCaptions",
                                                                                        duplicate.title))

        # Wenn eine Änderung vorgenommen wurde, werden alle Lines kombiniert
        if allGrafanaPanelsIndex > 0:
            completedTemplate = self.extendListWithList(data, allGrafanaPanelsIndex, dataLineContent)
            with open(temp_pdf_files + "/main.tex", "w") as f:
                for line in completedTemplate:
                    f.writelines(line)

        pdf_filename = 'main.pdf'
        pdf_temp_filepath = temp_pdf_files + '/' + pdf_filename
        self.pdfCompile(pdf_filename, temp_pdf_files)
        self.pdfCompile(pdf_filename, temp_pdf_files)

        with open(pdf_temp_filepath, 'rb') as fr:
            pdf_content = fr.read()

        pdf_final_directory = os.path.join(base_directory, pdf_uid + ".pdf")

        with open(pdf_final_directory, 'wb') as fw:
            fw.write(pdf_content)

        # Überprüft ob temporäres directory existiert und löscht es samt Inhalt
        if os.path.exists(temp_pdf_files):
            shutil.rmtree(temp_pdf_files)

        return str(pdf_uid) + ".pdf"


if __name__ == '__main__':
    image_reporter = ImageReporter()
    image_reporter.createPDF(os.getcwd())
